package nl.lolmewn.fbconnect;

import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.RawAPIResponse;
import facebook4j.auth.AccessToken;
import facebook4j.conf.ConfigurationBuilder;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

public class FBConnect extends JavaPlugin {

    private Facebook fb;
    private String app_id, app_secret, callback_url;
    
    @Override
    public void onEnable() {
        this.getDataFolder().mkdir();
        this.saveDefaultConfig();
        if (getConfig().getString("facebook.app_id", "0").equals("0")) {
            this.getLogger().severe("Config is not yet configured. Do this first, then restart your server.");
            this.setEnabled(false);
            return;
        }
        loadConfig();
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(getConfig().getBoolean("debug", false))
                .setOAuthAppId(this.app_id)
                .setOAuthAppSecret(this.app_secret)
                .setOAuthCallbackURL(this.callback_url)
                .setOAuthPermissions("publish_actions");
        this.fb = new FacebookFactory(cb.build()).getInstance();
        this.registerAPI();
    }
    
    public Facebook getFacebook(){
        return fb;
    }

    private void registerAPI() {
        this.getServer().getServicesManager().register(FacebookAPI.class, new FacebookAPI(this), this, ServicePriority.Low);
    }

    private void loadConfig() {
        this.app_id = getConfig().getString("facebook.app_id");
        this.app_secret = getConfig().getString("facebook.app_secret");
        this.callback_url = getConfig().getString("facebook.callback_url");
    }

}
