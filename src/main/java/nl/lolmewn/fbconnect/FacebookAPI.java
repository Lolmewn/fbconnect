package nl.lolmewn.fbconnect;

import facebook4j.Facebook;

/**
 *
 * @author Lolmewn
 */
public class FacebookAPI {
    
    private final FBConnect plugin;
    
    protected FacebookAPI(FBConnect main){
        this.plugin = main;
    }
    
    public Facebook getFacebook(){
        return plugin.getFacebook();
    }

}
